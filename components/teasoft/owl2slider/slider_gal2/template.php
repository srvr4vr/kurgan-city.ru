<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
	if ($arParams['OWL2SLIDER_COMPOSITE'] == 'Y') {
		if(method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin(GetMessage('OWL2SLIDER_COMPOSITE_INIT'));
	}
    if (!function_exists('JSParamValueBool')) {
        function JSParamValueBool($whatToCheck) {
            $jsBool = 'false';
            if ($whatToCheck) {
                if ($whatToCheck=='Y') {
                    $jsBool = 'true';
                }
            }
            return $jsBool;
        }
    }
?>

<?
//echo "<pre>", print_r($arResult['ITEMS']), "</pre>";
if (count($arResult['ITEMS']) > 0):?>
<div id="teasoft-owl-carousel-<?=$arParams['OWL2SLIDER_UNIQUE_SUFFIX'];?>" class="owl-carousel"> 
	<?foreach($arResult['ITEMS'] as $item):?> 
	<div class="teasoft-owl-item-content" id="<?=$this->GetEditAreaId($item['ID']);?>">
   <?if ($item['AD_TYPE'] == 'flash'):?>
			<a target="<?=$item['ITEM_URL_TARGET'];?>" href="<?=$item['ITEM_URL'];?>">
			<figure>
				<object type="application/x-shockwave-flash" data="<?=$item['PICTURE_RESIZED']['src'];?>" height="<?=$item['PICTURE_RESIZED']['height'];?>" width="<?=$item['PICTURE_RESIZED']['width'];?>">
					<param name="movie" value="<?=$item['PICTURE_RESIZED']['src'];?>" />
					<param name="quality" value="high"/>
					<param name="wmode" value="opaque" />
					<embed style="z-index:0;" src="<?=$item['PICTURE_RESIZED']['src'];?>" height="<?=$item['PICTURE_RESIZED']['height'];?>" width="<?=$item['PICTURE_RESIZED']['width'];?>" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" wmode="opaque">
				</object>
			</figure>
			</a>
	<?elseif ($item['AD_TYPE'] == 'html'):?>
		<?=$item['CODE'];?>
	<?else:?>
			<?


			$this->AddEditAction($item['ID'], $item['EDIT_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>		
			<? if($arParams['OWL2SLIDER_OWL_OPTS_video']=="N"):?>			

				<a href='<?=CFile::GetPath($item["PROPS"]["REAL_PICTURE"]["VALUE"]);?>'  data-lightbox="<?=$arParams['OWL2SLIDER_UNIQUE_SUFFIX'];?>" data-caption="<?=$item['ITEM_TEXT'];?>">

				<img class="teasoft-adaptive-img <?if($arParams['OWL2SLIDER_OWL_OPTS_lazyLoad']=="Y" && (count($arResult['ITEMS']) >=9)):?>owl-lazy<?endif;?>" title='<?=$item['ITEM_TEXT'];?>' alt='<?=($item['NAME']);?>' <?if($arParams['OWL2SLIDER_OWL_OPTS_lazyLoad']=="Y" && (count($arResult['ITEMS']) >=9)):?>data-<?endif;?>src="<?=$item['PICTURE_RESIZED']['src'];?>" <?if ($arParams['OWL2SLIDER_OWL_OPTS_merge']=="Y" && $item['ITEM_MERGE_COUNT']>1):?> data-merge="<?=$item['ITEM_MERGE_COUNT']; endif;?>" />

				</a>

			<?else:?>
				<?if ($item['ITEM_VIDEO_URL'] != ''):?>	

				<div class="teasoft-item-video" <?if ($arParams['OWL2SLIDER_OWL_OPTS_merge']=="Y" && $item['ITEM_MERGE_COUNT']>1):?> data-merge="<?=$item['ITEM_MERGE_COUNT']; endif;?>"><a class="owl-video" href="<?=$item['ITEM_VIDEO_URL'];?>"></a></div>
				<?else:?>
					<?if ($item['ITEM_URL'] != ''):?>
					<a target="<?=$item['ITEM_URL_TARGET'];?>"  href="<?=$item['ITEM_URL'];?>">
					<?endif;?>								
					<img class="teasoft-adaptive-img <?if($arParams['OWL2SLIDER_OWL_OPTS_lazyLoad']=="Y" && (count($arResult['ITEMS']) >=9)):?>owl-lazy<?endif;?>" title='<?=$item['ITEM_TEXT'];?>' alt='<?=($item['NAME']);?>' <?if($arParams['OWL2SLIDER_OWL_OPTS_lazyLoad']=="Y" && (count($arResult['ITEMS']) >=9)):?>data-<?endif;?>src="<?=$item['PICTURE_RESIZED']['src'];?>" <?if ($arParams['OWL2SLIDER_OWL_OPTS_merge']=="Y" && $item['ITEM_MERGE_COUNT']>1):?> data-merge="<?=$item['ITEM_MERGE_COUNT']; endif;?>" />
					<?if ($item['ITEM_URL'] != ''):?>
					</a>
					<?endif;?>
				<?endif;?>				
			<?endif;?>
	<?endif;?>

	<?if ($arParams['OWL2SLIDER_OWL_OPTS_SHOW_DESCRIPTION'] == 'Y'):?>
	<span class="teasoft-owl-carousel-item-descr">
		<b><?=$item['NAME'];?></b><br/>
		<?if ($item['NAME'] != $item['ITEM_TEXT']) {
			echo $item['ITEM_TEXT'];;
		}
		?>
	</span>
	<?endif;?>	
	</div>
	<?endforeach;?>
</div>


<script type="text/javascript">
 $(document).ready(function () {
                $('#teasoft-owl-carousel-<?=$arParams['OWL2SLIDER_UNIQUE_SUFFIX'];?>').owlCarousel({
                    items : <?=$arParams['OWL2SLIDER_OWL_OPTS_VISIBLE_ITEMS_COUNT'];?>,
                    margin : <?=$arParams['OWL2SLIDER_OWL_OPTS_MARGIN'];?>,
                    loop : <?if($arParams['OWL2SLIDER_OWL_OPTS_LOOP']=="Y") {echo "true";} else {echo "false";}?>,
                    center : <?if($arParams['OWL2SLIDER_OWL_OPTS_center']=="Y") {echo "true";} else {echo "false";}?>,
                    mouseDrag : <?if($arParams['OWL2SLIDER_OWL_OPTS_mouseDrag']=="Y") {echo "true";} else {echo "false";}?>,
                    touchDrag : <?if($arParams['OWL2SLIDER_OWL_OPTS_touchDrag']=="Y") {echo "true";} else {echo "false";}?>,
                    pullDrag : <?if($arParams['OWL2SLIDER_OWL_OPTS_pullDrag']=="Y") {echo "true";} else {echo "false";}?>,
                    freeDrag : <?if($arParams['OWL2SLIDER_OWL_OPTS_freeDrag']=="Y") {echo "true";} else {echo "false";}?>,
                    stagePadding : <?=$arParams['OWL2SLIDER_OWL_OPTS_stagePadding']?>,
                    merge : <?if($arParams['OWL2SLIDER_OWL_OPTS_merge']=="Y") {echo "true";} else {echo "false";}?>,
                    mergeFit : <?if($arParams['OWL2SLIDER_OWL_OPTS_mergeFit']=="Y") {echo "true";} else {echo "false";}?>,
                    autoWidth: <?if($arParams['OWL2SLIDER_OWL_OPTS_autoWidth']=="Y") {echo "true";} else {echo "false";}?>,
                    autoHeight:<?if($arParams['OWL2SLIDER_OWL_OPTS_autoHeight']=="Y") {echo "true";} else {echo "false";}?>,        
                    startPosition: <?=$arParams['OWL2SLIDER_OWL_OPTS_startPosition']?>,
                    nav : <?if($arParams['OWL2SLIDER_OWL_OPTS_nav']=="Y") {echo "true";} else {echo "false";}?>,
                    navRewind : <?if($arParams['OWL2SLIDER_OWL_OPTS_navRewind']=="Y") {echo "true";} else {echo "false";}?>,
                    navText : [<?=implode(', ',htmlspecialcharsBack($arParams['OWL2SLIDER_OWL_OPTS_navText'])); ?>], 
                    slideBy : <?=$arParams['OWL2SLIDER_OWL_OPTS_slideBy']?>,         
                    dots : <?if($arParams['OWL2SLIDER_OWL_OPTS_dots']=="Y") {echo "true";} else {echo "false";}?>,
                    dotsEach : <?if($arParams['OWL2SLIDER_OWL_OPTS_dotsEach']=="Y") {echo "true";} else {echo "false";}?>,
                    <? if ((count($arResult['ITEMS']) >=9)):?>        
                    lazyLoad : <?if($arParams['OWL2SLIDER_OWL_OPTS_lazyLoad']=="Y") {echo "true";} else {echo "false";}?>,
                    <?endif;?>        
                    autoplay : <?if($arParams['OWL2SLIDER_OWL_OPTS_autoplay']=="Y") {echo "true";} else {echo "false";}?>,
                    autoplayTimeout : <?=$arParams['OWL2SLIDER_OWL_OPTS_autoplayTimeout']?>,  
                    autoplayHoverPause : <?if($arParams['OWL2SLIDER_OWL_OPTS_autoplayHoverPause']=="Y") {echo "true";} else {echo "false";}?>,
                    smartSpeed : <?if(intval($arParams['OWL2SLIDER_OWL_OPTS_smartSpeed'])>0) {echo $arParams['OWL2SLIDER_OWL_OPTS_smartSpeed'];} else {echo "false";}?>,
                    autoplaySpeed : <?if(intval($arParams['OWL2SLIDER_OWL_OPTS_autoplaySpeed'])>0) {echo $arParams['OWL2SLIDER_OWL_OPTS_autoplaySpeed'];} else {echo "false";}?>,
                    navSpeed : <?if(intval($arParams['OWL2SLIDER_OWL_OPTS_navSpeed'])>0) {echo $arParams['OWL2SLIDER_OWL_OPTS_navSpeed'];} else {echo "false";}?>,
                    dotsSpeed : <?if(intval($arParams['OWL2SLIDER_OWL_OPTS_dotsSpeed'])>0) {echo $arParams['OWL2SLIDER_OWL_OPTS_dotsSpeed'];} else {echo "false";}?>,
                    dragEndSpeed : <?if(intval($arParams['OWL2SLIDER_OWL_OPTS_dragEndSpeed'])>0) {echo $arParams['OWL2SLIDER_OWL_OPTS_dragEndSpeed'];} else {echo "false";}?>,                    
                    <?if ($arParams['OWL2SLIDER_OWL_OPTS_responsive']=="Y"):?>
					responsiveClass : true,
                    responsive : {  
                        0: {//Extra small devices
                        	items:1                          
                        },                      
                        768: {//Small devices Tablets 
                        	items:2                          
                        },                        
                        992: {//Medium devices Desktops
                        	items:3                           
                        },                        
                        1200: {//Large devices Desktops 
                        	items:3                         
                        },
                    },
                    responsiveRefreshRate: <?=$arParams['OWL2SLIDER_OWL_OPTS_responsiveRefreshRate']?>, 
                    responsiveBaseElement: <?=$arParams['OWL2SLIDER_OWL_OPTS_responsiveBaseElement']?>,  
                   <?else:?>
					responsive : false,
                    <?endif;?>
                    
                    video : <?if($arParams['OWL2SLIDER_OWL_OPTS_video']=="Y") {echo "true";} else {echo "false";}?>,
                    videoHeight : <?if(intval($arParams['OWL2SLIDER_OWL_OPTS_videoHeight'])>0) {echo  $arParams['OWL2SLIDER_OWL_OPTS_videoHeight'];} else {echo "false";}?>, 
                    videoWidth : <?if(intval($arParams['OWL2SLIDER_OWL_OPTS_videoWidth'])>0) {echo $arParams['OWL2SLIDER_OWL_OPTS_videoWidth'];} else {echo "false";}?>,
                    animateOut : <?if($arParams['OWL2SLIDER_OWL_OPTS_animateOut']=="Y") {echo '"',$arParams['OWL2SLIDER_OWL_OPTS_animateOut_Type'],'"';} else {echo "false";}?>,
                    animateIn : <?if($arParams['OWL2SLIDER_OWL_OPTS_animateIn']=="Y") {echo '"', $arParams['OWL2SLIDER_OWL_OPTS_animateIn_Type'],'"';} else {echo "false";}?>,
                    fallbackEasing : '<?=$arParams['OWL2SLIDER_OWL_OPTS_fallbackEasing']?>',
                    rtl : <?if($arParams['OWL2SLIDER_OWL_OPTS_rtl']=="Y") {echo "true";} else {echo "false";}?>,
                      
                });

                //responsive for flash
                var flashWrapItems = $('.teasoft-owl-item-content');
                var flashItems = flashWrapItems.find("object, embed");
                var flashFluidItems = flashWrapItems.find('figure');

                if (flashWrapItems.length) {
                    flashItems.each(function() {
                        $(this)
                            // jQuery .data does not work on object/embed elements
                            .attr('data-aspectRatio', this.height / this.width)
                            .removeAttr('height')
                            .removeAttr('width');
                    });

                    $(window).resize(function() {
                        var newWidth = flashFluidItems.width();
                        flashItems.each(function() {
                            var $el = $(this);
                            $el
                                .width(newWidth)
                                .height(newWidth * $el.attr('data-aspectRatio'));
                        });
                    }).resize();
                }

 
});
 
$(window).load(function() {
     $('.teasoft-item-video').css("height" , function(){
         var H=$(this).height();
         if (H==0) H = $(this).closest('.owl-stage').height();
         return H + "px";
         });     
});
</script>
<?endif;?>

<?if ($arParams['OWL2SLIDER_COMPOSITE'] == 'Y'):?>
	<?if(method_exists($this, 'createFrame')) $frame->end();?>
<?endif;?>