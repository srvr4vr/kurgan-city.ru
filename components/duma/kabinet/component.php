<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

$cYear 			= isset($_GET['year'])  && is_numeric($_GET['year'])  ? $_GET['year'] : 0; //���
$cMonth			= isset($_GET['month']) && is_numeric($_GET['month']) ? $_GET['month'] : 0;//�����
$mode 			= isset($_GET['mode']) ? $_GET['mode']:0; //�����
$cDir 			= isset($_GET['dir']) ? $_GET['dir']:0;   //����������


if($_GET['clear_cache']=="Y")
{
   $obCache = \Bitrix\Main\Data\Cache::createInstance();
   $obCache->clean("kabinet.index","/");
   $obCache->clean("kabinet.meetings".date('Y').date('n'),"/");
   $obCache->clean("kabinet.monthes".date('Y'),"/");
   $obCache->clean("kabinet.arhive","/");
}

$APPLICATION->IncludeComponent("duma:kabinet.tools","",	array(),$component);

$componentPages = array
(
	0 => "index",
	1 => "arhive",
	2 => "monthes",
	3 => "meetings",
	4 => "docs",
);

$componentPage = $componentPages[0];
if($cYear!=0)
{
	if ($cMonth!=0){
		$componentPage= $componentPages[3];
	}else{
		$componentPage= $componentPages[2];
	}
}

if($cDir != 0 ) $componentPage= $componentPages[4];

if($mode === "arhive") $componentPage= $componentPages[1]; 
$this->includeComponentTemplate($componentPage);