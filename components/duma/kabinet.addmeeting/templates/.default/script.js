window.onload = function() { 
console.log(templatePath);

//templatePath = "/local/components/duma/kabinet.addmeeting";

var buttonDiv = $('#buttonDiv')[0];

var dropArea = document.getElementById('dropZone');
var destinationUrl = componentPath + "/uploader.php";
console.log(destinationUrl);
var result = $('#infoBox');
var mainLebel = $('#mainLebel')[0];
var mainLebelValue = mainLebel.innerHTML;

var secondaryLebel = $('#secondaryLebel')[0];
var secondaryLebelValue = secondaryLebel.innerHTML;

var areaDefaultValue = dropArea.innerHTML;
var list = [];
var totalSize = 0;
var totalProgress = 0;
var folder = "";

var okButton = $('#okBtn')[0];
var cancelButton = $('#cancelBtn')[0];

var fileImagePath  =  templatePath + "/images/doc-ico.png"
var loaderGifPath  =  templatePath + "/images/loader.gif"
var errorImagePath =  templatePath + "/images/error.png"
var checkImagePath =  templatePath + "/images/check.png"


function ClearPartial(){
   mainLebel.innerHTML=mainLebelValue;
   secondaryLebel.innerHTML=secondaryLebelValue;
   dropArea.className = '';
   buttonDiv.className = 'hiddenDiv';
   list = [];
   folder= "";
}

function ClearAll(){
   ClearPartial();
   result.empty();
}

function DateError() {
   mainLebel.innerHTML="��������";
   secondaryLebel.innerHTML="�� ��������� �������� �������!";
   dropArea.className = 'error';
   buttonDiv.className = 'visibleDiv';
}

    // init handlers
function initHandlers() {
   dropArea.addEventListener('drop', handleDrop, false);
   dropArea.addEventListener('dragover', handleDragOver, false);
   okButton.onclick = OkClick;
   cancelButton.onclick = CancelClick;
}

// drag over
function handleDragOver(event) {
   event.stopPropagation();
   event.preventDefault();
   mainLebel.innerHTML=mainLebelValue;
   secondaryLebel.innerHTML=secondaryLebelValue;
   dropArea.className = 'hover';
}

// drag drop
function handleDrop(event) {
   event.stopPropagation();
   event.preventDefault();
   dropArea.className="";
   processFiles(event.dataTransfer.files);
}

function OkClick() {
   uploadNext();
}

function CancelClick() {
   ClearAll();
}

    // process bunch of files
function processFiles(filelist) {
   ClearAll();
   if (!filelist || !filelist.length || list.length) return;
   
   for(var i = 0; i < filelist.length ; i++) {
      var filename = filelist[i].name;
      if (filename.substring(filename.length-3)=="csv")
      folder = filename.substring(0,filename.length-4);
   }

   if (!folder || 0 === folder.length) {
      dropArea.className ="error";

      mainLebel.innerHTML="��� csv �����";
      secondaryLebel.innerHTML="���������� ������� ���������";
      return;
   }

   var dateText = folder.substring(0,10);
   for (var i = 0; i < filelist.length; i++) {
      filelist[i].id=i;
      list.push(filelist[i]);
      var str = '<div id="uItem'+i+'" class="fileItem" title="'+filelist[i].name+'"><div class="fileImg"><img id="uImage'+i+'" class="indicator" src="'+loaderGifPath+'"/></div><br/><p class="itemName">'+filelist[i].name+'</p></div>';
      result.append(str);
   }


   if (CheckDay(dateText)){
      DateError();
      return;
   }
   uploadNext();
}

function CheckDay(dateText){
   var date = Date.parse(dateText);
   var dateNow = Date.now();
   var deltaTime = dateNow - date;
   var days = deltaTime / (1000*60*60*24);
   return days> 1;
}

// on complete - start next file
function handleComplete(id) {
   var result = JSON.parse(id);
   var item = $('#uImage'+result.id);
   item.attr('src', checkImagePath);
   uploadNext();
}

function handleBigFileError(id){
   var item = $('#uImage'+id);
   var itemTitle = $('#uItem'+id);
   var oldTitle= itemTitle.attr('title');
   itemTitle.attr('title', oldTitle+ " - ������� �������");
   item.attr('src', errorImagePath);
   uploadNext();
}

function handleError(id, event){
   var item = $('#uImage'+id);
   item.getAttribute();
   item.attr('src', errorImagePath);
   uploadNext();
}


    // upload file
function uploadFile(file, status, index) { 
   var formData = new FormData();
   formData.append(file.name, file, file.name); 
   $.ajax({
      url: destinationUrl +'?id='+index+'&folder='+folder, // point to server-side PHP script 
	  dataType: 'text',  // what to expect back from the PHP script, if anything
	  cache: false,
	  contentType: false,
	  processData: false,
	  data: formData,
	  type: 'post',
	  success: function(php_script_response){handleComplete(php_script_response);}
   });
}

// upload next file
function uploadNext() {
   if (list.length) {
      dropArea.className = 'uploading';
	  var nextFile = list.shift();
	  if (nextFile.size >= 20*1024*1024) {
	     handleBigFileError(nextFile.id);
      }
	  else {
         uploadFile(nextFile, status, nextFile.id);
      }
    } 
	else {
       ClearPartial();
 		console.log("�������� ������� ����");
		console.log(componentPath + "/reset-cache.php");
       $.ajax({
			url: componentPath + "/reset-cache.php",
			success: function(php_script_response){console.log("��� ������ "+php_script_response);}
		});
       //location.reload(true);
		location = "./?clear_cache=Y";
    }
}

   initHandlers();
};