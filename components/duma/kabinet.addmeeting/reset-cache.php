
<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");?>
<?
   $obCache = \Bitrix\Main\Data\Cache::createInstance();
   $obCache->clean("kabinet.index","/");
   $obCache->clean("kabinet.meetings".date('Y').date('n'),"/");
   $obCache->clean("kabinet.monthes".date('Y'),"/");
   $obCache->clean("kabinet.arhive","/");
   echo true;
   return true;
?>