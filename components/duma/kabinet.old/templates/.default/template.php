<!--�������� ������� ������� ��������-->
<!--������� �.�., 03'2017-->

<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$this->addExternalCss("style.css");
$monthAr = array(
    1 => '������',
    2 => '�������',
    3 => '����',
    4 => '������',
    5 => '���', 
    6 => '����',
    7 => '����', 
    8 => '������',
    9 => '��������',
    10=> '�������', 
    11=> '������', 
    12=> '�������');

function WriteMeeting ($path, $daysArray, $month)
{?>
	<div>
		<ul>
		<?foreach ($daysArray as $day):?>
			<a href="<?=$pathcurrent?>?mode=3&dir=<?=$day[0]?>&month=<?=$month?>"><li><p><?=$day[4]?></p></li></a>
		<?endforeach;?>
		</ul>
	</div>
<?}

function WriteMainPage($days, $pathcurrent, $days)
{?>
	<? if (count($days)>0): ?>
	<hr class="delim" />
		<p><b>������� ���������:</b></p>
		<? WriteMeeting($pathcurrent,$days);?>
	<? endif; ?>
<?}

function WriteMonth($monthes, $pathcurrent, $cYear, $monthAr)
{?>
	<div class=flex-container>
	<?foreach ($monthes as $month):?>
		<a href="<?=$pathcurrent?>?mode=2&year=<?=$cYear?>&month=<?=$month?>"><div class="white-box primary-border-box"><p><?=$monthAr[(int)$month]?></p></div></a>
	<?endforeach;?>
	</div><?
}

function WriteYears($years, $pathcurrent)
{?>
	<div class=flex-container>
		<?foreach ($years as $year):?>
			<a href="<?=$pathcurrent?>?mode=1&year=<?=$year?>"><div class="white-box primary-border-box"><p><?=$year?></p></div></a>	
		<?endforeach;?>
	</div>
<?
}

function WriteDocs($files, $cDir) {?>
	<div class="clear top-pad">
		<ol>	
		<?php foreach ($files as $file):?>
			<? if(!IsNullOrEmptyString($file[1]) and $file[0]!="Section" and $file[0]!="Date"): ?>
				<? if(IsNullOrEmptyString($file[0])): ?>
					<li><p><?=$file[1]?></p></li>
				<?php else:?>
					<a href="/kabinet/<?=$cDir?>/<?=$file[0]?>"><li><p><?=$file[1]?></p></li></a>
				<? endif; ?>
			<? endif; ?>
		<?endforeach;?>
		</ol>
	</div>
	<?
}

//������������ ��������� ��������
$mode=$arResult['mode'];
$cYear=$arResult['cYear'];
$cMonth=$arResult['cMonth'];
$cDay=$arResult['cDay'];
$cDir=$arResult['cDir'];
$docs=$arResult['docs'];
$pathcurrent=$arResult['pathcurrent'];

$monthes=$arResult['monthes'];
$days=$arResult['days'];
$files=$arResult['files'];
$days=$arResult['days'];
$years=$arResult['years'];

$title="";
?>

<? if ($mode==0): ?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "current.php",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
<?php endif; ?>

<div class="mainBody clear">
<?
switch ($mode) {
    case 1:
		$title =$cYear . ' ���';
		$APPLICATION->SetTitle($title);
		$APPLICATION->AddChainItem("�����", "./index.php?mode=4");
        WriteMonth($monthes, $pathcurrent, $cYear, $monthAr);
        break;
    case 2:
		$title =$monthAr[(int)$cMonth] . ' ' . $cYear . " ����";
		$APPLICATION->SetTitle($title);
		$APPLICATION->AddChainItem('�����', './index.php?mode=4');
		$APPLICATION->AddChainItem($cYear . ' ���','./index.php?mode=1&year='.$cYear);
        WriteMeeting($pathcurrent, $days, $cMonth);
        break;
	case 3:
		$title =$files[0][1];
		$APPLICATION->SetTitle($title);
		$APPLICATION->AddChainItem('�����', './index.php?mode=4');
		$APPLICATION->AddChainItem($cYear . ' ���','./index.php?mode=1&year='.$cYear);
		$title2 =$monthAr[(int)$cMonth] . ' ' . $cYear . " ����";
		$APPLICATION->AddChainItem($title2,'./index.php?mode=2&year='.$cYear.'&month='.$cMonth);
        WriteDocs($files, $cDir);
        break;
	case 4:
        $title ='�����';
		$APPLICATION->SetTitle($title);
        WriteYears($years, $pathcurrent);
        break;
	default:
		$title = "������� ��������";
		$APPLICATION->SetTitle($title);
        WriteMainPage($days, $pathcurrent, $days);
        break;
}
?>



<!--������� � ������� ����-->

<div class="page-navigation-wrapper page-navigation-pad">
    <div class="page-navigation">
        <span class="page-navigation-pages">
		<? for ($i = 0; $i <4; $i++):?>
			<?$type = (($cYear===$years[$i]) && (!($mode==4) && !($mode==0)))? "current": "";?>
			<a class="<?=$type?>" href="<?=$pathcurrent?>?mode=1&amp;year=<?=$years[$i]?>"><?=$years[$i]?></a>
		<?endfor;?>
		<?$ttype = (($mode==4) or ($cYear<=$years[$i]))? " current": ""; ?>
			<a title="�����" class="<?=$ttype?>" href="<?=$pathcurrent?>?mode=4"><b>&middot;&middot;&middot;</b></a>
		</span>
    </div>
</div>
</div>
<?
if ($arResult['isEditor'] )
{
	$APPLICATION->IncludeComponent("duma:kabinet.addmeeting","",Array(),false);
}
?>