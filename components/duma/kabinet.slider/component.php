<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?php

$dir = isset($_GET['dir']) ? $_GET['dir'] : "2008-01-01";
$y = substr($dir,0,4);
$arResult['YEAR'] = isset($_GET['year'])  && is_numeric($_GET['year'])  ? $_GET['year'] : $y; 
$this->IncludeComponentTemplate();
?>
