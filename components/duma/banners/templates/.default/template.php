<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="double-header-block clearfix col-margin-bottom"> 
  <h2 class="double-header"><?echo $arParams["TITLE"] ?></h2>
 </div>


<div class="white-box padding-box primary-border-box pt0"> 
	<div class="LinkContainer clearfix">

		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>


			<div id="<?=$this->GetEditAreaId($arItem['ID']);?>">

				<?php if(!empty($arItem["PROPERTY_LINK_VALUE"])):?>
					<a title="<?=$arItem["NAME"]?>" href="<?=$arItem["PROPERTY_LINK_VALUE"]?>">
				<?php endif;?>
				<img border="0" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>"/>
				<?php if(!empty($arItem["PROPERTY_LINK_VALUE"])):?>
					</a>
				<?php endif;?>
			</div>
		<?endforeach;?>
	</div>
</div>
