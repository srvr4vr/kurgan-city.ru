<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Application;
use Bitrix\Main\Web\Uri;

$monthAr = array(
    1 => '������',
    2 => '�������',
    3 => '����',
    4 => '������',
    5 => '���', 
    6 => '����',
    7 => '����', 
    8 => '������',
    9 => '��������',
    10=> '�������', 
    11=> '������', 
    12=> '�������');


$request = Application::getInstance()->getContext()->getRequest();
$uriString = $request->getRequestUri();
$uri = new Uri($uriString);
$pathcurrent = $uri->getPath(); 


$this->setFrameMode(true);
$APPLICATION->SetTitle($arResult['year'] . ' ���');
$APPLICATION->AddChainItem("�����", "./index.php?mode=arhive");
?>
<div class=flex-container>
	<?foreach ($arResult['monthes'] as $month):?>
	<a href="./?year=<?=$arResult['year']?>&month=<?=$month?>"><div class="white-box primary-border-box"><p><?=$monthAr[(int)$month]?></p></div></a>
	<?endforeach;?>
</div>

<?$APPLICATION->IncludeComponent(
	"duma:kabinet.slider", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"DOCS" => $arParams['DOCS']
	),
	$component
);?>

