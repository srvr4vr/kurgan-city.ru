<?
//���������� ������� ��� �������� ��������. �2017 ������� �.�.

//�������� ��� ��������� ���� �� ������
function GetYearFolder($dir)
{
	$filteredArray = array();
	foreach (glob($dir."/*") as $filename)
	{
		$name =substr($filename,strlen($dir)+1,4);
     $filteredArray[] = $name;
	}
	$years = array_unique( $filteredArray );
    rsort( $years );
	return $years;
}

//������ ������������ � ������ �� ��������� ���
function GetMonthFolder($year, $dir)
{
	$filteredArray = array();
	$path = $dir."/".$year."*";
	foreach (glob($path) as $filename)
    $filteredArray[] = substr($filename,strlen($dir)+6,2);
	$monthes = array_unique( $filteredArray );
    sort( $monthes );

	return $monthes;
}


//�������� ������ ��������� �� �����
function GetDays($year, $month, $dir)
{
	$filteredArray = array();
	$month= sprintf('%02d', $month);
	$p= $dir."/".$year."-".$month."*";
	foreach (glob($p) as $filename)
	{

     $day = substr($filename,strlen($dir)+8,2);
	 $com = substr($filename,strlen($dir)+11);
	 $fol = substr($filename,strlen($dir)+1);
	 $cFolder = $dir."/".$fol."/".$fol.".csv";
	 if(!file_exists($cFolder)) continue;
	 $t = GetCSV($cFolder);
     $filteredArray[] =array( $fol ,$day,$com, $cFolder,$t[0][1]);
	}
	sort( $filteredArray );
	return $filteredArray;
}

//�������� ������ ��������� �� ������� ����
function GetDays2($year, $dir)
{
	$filteredArray = array();
	foreach (glob($dir."/".$year."*") as $filename)
	{
	 $mon =  substr($filename,strlen($dir)+6,2);
     $day = substr($filename,strlen($dir)+9,2);
	 $com = substr($filename,strlen($dir)+12);
	 $fol= substr($filename,strlen($dir)+1);
	 $cFolder = $dir."/".$fol."/".$fol.".csv";

	 if(!file_exists($cFolder)) continue;

	 $delta = strtotime($year."-".$mon."-".$day); 

	 $cDelta = strtotime(date("Y")."-".date("n")."-".date("d")); 
	 if(($delta>=$cDelta)){
	 		$t = GetCSV($cFolder);
			$filteredArray[] =array( $fol ,$day,$com, $cFolder,$t[0][1]);
     }
	}
	sort( $filteredArray );

	return $filteredArray;

}



function IsNullOrEmptyString($question){
    return (!isset($question) || trim($question)==='');
}


//������ CSV
function GetCSV($file)
{
    $ar= array();
	$handle = fopen ($file,"r");

	while (($data = fgetcsv($handle, 1000,';')) !== FALSE) 
    {
		if(!IsNullOrEmptyString($data[1]))  $ar[]=array($data[0], $data[1]);
    }
    fclose($handle);
	return  $ar;
}
?>
