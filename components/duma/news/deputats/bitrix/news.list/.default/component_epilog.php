<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


function intToRomenumber($num)
{
	$numbers = Array( 1000,  900,  500,  400,  100,   90,  
                                         50,   40,   10,    9,    5,    4,    1 );
	$letters = Array( "M",  "CM",  "D",  "CD", "C",  "XC",
                                         "L",  "XL",  "X",  "IX", "V",  "IV", "I" );

	$roman = "";  // The roman numeral.
    $N = $num;        // N represents the part of num that still has

    for ($i = 0; $i < count($numbers); $i++) 
    {
    	while ($N >= $numbers[$i])
		{ 
        	$roman = $roman.$letters[$i];

            $N -= $numbers[$i];
        }
    }
    return $roman;
}

global $APPLICATION;
$section = $arResult["SECTION"]["PATH"][0]["CODE"];
$startDate = 2004 + ($section-4)*5;
$stopDate = 2009 + ($section-4)*5;
$APPLICATION->SetTitle("�������� ���������� ��������� ���� ".intToRomenumber($section)." ������. ".$startDate."-".$stopDate." ��.");
$APPLICATION->SetTitle("f") ;
?> 