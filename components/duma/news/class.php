<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class CDumaListComponent extends CBitrixComponent
{
    function intToRomenumber($num)
	{
		$numbers = Array( 1000,  900,  500,  400,  100,   90,  
                                         50,   40,   10,    9,    5,    4,    1 );
		$letters = Array( "M",  "CM",  "D",  "CD", "C",  "XC",
                                         "L",  "XL",  "X",  "IX", "V",  "IV", "I" );

		$roman = "";  // The roman numeral.
    	$N = $num;        // N represents the part of num that still has

    	for ($i = 0; $i < count($numbers); $i++) 
    	{
    		while ($N >= $numbers[$i])
			{ 
        		$roman = $roman.$letters[$i];

            	$N -= $numbers[$i];
        	}
    	}
    	return $roman;
	}

	public function GetSectionName($sectionCode)
	{
		$startDate = 2004 + ($sectionCode-4)*5;
		$stopDate = 2009 + ($sectionCode-4)*5;
		return "�������� ���������� ��������� ���� ".$this->intToRomenumber($sectionCode)." ������. ".$startDate."-".$stopDate." ��.";
	}

}?>