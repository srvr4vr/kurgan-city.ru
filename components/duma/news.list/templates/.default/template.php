<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
function writePicture($arItem)
{
	echo '<img class="preview_picture" border="0" src="'.$arItem["PREVIEW_PICTURE"]["SRC"]
		.'"alt="'.$arItem["PREVIEW_PICTURE"]["ALT"].'" title="'.$arItem["NAME"].'" width="'
		.$arItem["PREVIEW_PICTURE"]["WIDTH"].'" height="'.$arItem["PREVIEW_PICTURE"]["HEIGHT"].'" />';
}

function writeName($arItem)
{
  if(0==0 && (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])))
  {
		echo '<h2><a href="'.$arItem["DETAIL_PAGE_URL"].'">'.$arItem["NAME"].'</a></h2>';
  }
  else
  {
		echo '<h2>'.$arItem["NAME"].'</h2>';
  }
}

function writeInfo($arItem)
{
	if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"])
	{
		echo $arItem["PREVIEW_TEXT"];
	}
}

function writeFields($arItem)
{
	if(isset($arItem["PROPERTIES"]) && !empty($arItem["PROPERTIES"]) )
	{
		echo "<div class='top-pad'>";
		$tel= $arItem["PROPERTIES"]["TELEPHONE"]["VALUE"];
		if(isset($arItem["PROPERTIES"]["TELEPHONE"]["VALUE"]) && !empty($tel))
		{
			echo "<div>���.: <strong>".$tel."</strong></div>";
		}
		$email = $arItem["PROPERTIES"]["EMAIL"]["VALUE"];
		if(isset($arItem["PROPERTIES"]["EMAIL"]["VALUE"]) && !empty($email))
		{
			echo '<div>e-mail: <a href="mailto:'.$email.'"><strong>'.$email.'</strong></a></div>';
		}
		$site = $arItem["PROPERTIES"]["SITE"]["VALUE"];
		if(isset($arItem["PROPERTIES"]["SITE"]["VALUE"]) && !empty($site))
		{
			echo '<div>����: <a href="'.$site.'"><strong>'.$site.'</strong></a></div>';
		}
		echo "</div>";
	}
}
?>
<div class="news-list">
	<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
		$okrug = $arItem["DISPLAY_PROPERTIES"]["OKRUG"]["DISPLAY_VALUE"];
		$soziv = $arItem["DISPLAY_PROPERTIES"]["SOZIV"]["DISPLAY_VALUE"];
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

	<div class="white-box padding-box flex-box primary-border-box" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<div class="flex-subbox first-block">
			<div class="box"><? writePicture($arItem) ?></div>
			<div class="box top-pad">
				<div><a href="/duma/dnevnik/<?= $arItem['PROPERTIES']['DNEVNIK']['VALUE']?>/">�������</a></div>
				<div><a href="/duma/tribuna/<?= $arItem['PROPERTIES']['TRIBUNA']['VALUE']?>/">�������</a></div>
				<div><a href="/duma/blagodar/<?= $arItem['PROPERTIES']['BLAGODAR']['VALUE']?>/">�������������</a></div>
			</div>
		</div>
		<div class="flex-subbox second-block">
			<div class="box">
				<? writeName($arItem); ?>
				<div class="top-pad" align="center"><a href="/city/dom/?sec=izb&okr=<?=$okrug?>">����� � <?=$okrug?></a></div>
			</div>
			<div class="box top-pad">
				<? writeInfo($arItem) ?>
				<? writeFields($arItem) ?>
			</div>
		</div>
	</div>
	<?endforeach;?>
</div>