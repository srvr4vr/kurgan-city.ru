<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?php

//�������� ������� ������� ��������
//������� �.�., 03'2017

$APPLICATION->IncludeComponent("duma:kabinet.tools","",	array(),$component);


//�������� ��������� �� �������� ������
$arResult['year'] 	= isset($_GET['year'])  && is_numeric($_GET['year'])  ? intval($_GET['year']) : date("Y"); //���
$arResult['month'] 	= isset($_GET['month'])  && is_numeric($_GET['month'])  ? intval($_GET['month']) : date("n"); //�����


if ($arResult['year'] >= 2099)
   	$arResult['year'] = 2099;

if ($arResult['year'] <= 2008)
   	$arResult['year'] = 2008;

if ($arResult['month'] >= 12)
   	$arResult['month'] = 12;

if ($arResult['month'] <= 1)
   	$arResult['month'] = 1;

$�ache = Bitrix\Main\Data\Cache::createInstance();
if ($�ache->initCache(86400, "kabinet.meetings".$arResult['year'].$arResult['month'],"/"))
{
    $arResult = $�ache->getVars();
}
elseif ($�ache->startDataCache())
{
	$arResult['DOCS'] = $_SERVER['DOCUMENT_ROOT']."/".trim($arParams['DOCS']);
	$arResult['days'] = GetDays($arResult['DOCS'], $arResult['year'], $arResult['month']);
    $�ache->endDataCache($arResult);
} 

$this->IncludeComponentTemplate();
?>
