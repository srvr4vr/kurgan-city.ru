<?
$MESS["OWL2SLIDER_SOURCE_COUNT"] = "���-�� ���������";

$MESS["OWL2SLIDER_USE_JQUERY"] = "�������� jQuery �� ����������";
$MESS["OWL2SLIDER_EXPERT_MODE"] = "�������� ���������� ����� ��������";


// - GROUPS Settings Options
$MESS["OWL2SLIDER_DATA_SOURCE_GP"] = "��������� ������";
$MESS["OWL2SLIDER_SORTIROVKA_ELEMENTOV"] = "���������� ���������";
$MESS["OWL2SLIDER_DATA_RESIZING_GP"] = "���������� ����� ������� �����������";
$MESS["OWL2SLIDER_DATA_RESPONSIVE_GP"] = "Responsive �����";
$MESS["OWL2SLIDER_VISUAL_GP"] = "����� ����������� ��������";
$MESS["OWL2SLIDER_SINGLE_ITEM_GP"] = "���������� ����� ��� ���������� ��������";
$MESS["OWL2SLIDER_VIDEO_OPTS_GP"] = "���������� ����� ��� Video ���������";
$MESS["OWL2SLIDER_NAV_OPTS_GP"] = "���������� ����� ��� ��������� ��������";
$MESS["OWL2SLIDER_MERGE_OPTS_GP"] = "���������� ����� ������������ ���������";


// - Datasource Options

$MESS["OWL2SLIDER_VSE_TIPY"] = "��� ����";
$MESS["OWL2SLIDER_NE_VYBRANO1"] = "�� �������";
$MESS["OWL2SLIDER_VYBRATQ"] = "�������..";
$MESS["OWL2SLIDER_VSE_RAZDELY"] = "��� �������";

$MESS["OWL2SLIDER_REJIM"] = "�����";
$MESS["OWL2SLIDER_COMPOSITE"] = "����������� �����";
$MESS["OWL2SLIDER_LINK_URL_PROPERTY_ID"] = "�������� �������� ������ ��� �������";
$MESS["OWL2SLIDER_INCLUDE_SUBSECTIONS"] = "������� �������� �������";

$MESS["OWL2SLIDER_PO_VOZRASTANIU"] = "�� �����������";
$MESS["OWL2SLIDER_PO_VOZRASTANIU_PUST"] = "�� �����������, ������ �������";
$MESS["OWL2SLIDER_PO_VOZRASTANIU_PUST1"] = "�� �����������, ������ � �����";
$MESS["OWL2SLIDER_PO_UBYVANIU"] = "�� ��������";
$MESS["OWL2SLIDER_PO_UBYVANIU_PUSTYE"] = "�� ��������, ������ �������";
$MESS["OWL2SLIDER_PO_UBYVANIU_PUSTYE1"] = "�� ��������, ������ � �����";


$MESS["OWL2SLIDER_SORTBY_SORT"] = "����.";
$MESS["OWL2SLIDER_SORTBY_POSLEDNEE_IZMENENIE"] = "��������� ���������";
$MESS["OWL2SLIDER_SORTBY_NAZVANIE"] = "��������";
$MESS["OWL2SLIDER_SORTBY_DATA_NACALA_AKTIVNOS"] = "���� ������ ����������";
$MESS["OWL2SLIDER_SORTBY_DATA_OKONCANIA_AKTIV"] = "���� ��������� ����������";
$MESS["OWL2SLIDER_SORTBY_VREMA_PERVOGO_POKAZA"] = "����� ������� ������ ��������";
$MESS["OWL2SLIDER_SORTBY_USREDNENNOE_KOLICEST"] = "����������� ���������� �������";
$MESS["OWL2SLIDER_SORTBY_SLUCAYNYM_OBRAZOM"] = "��������� �������";

$MESS["OWL2SLIDER_SVOYSTVO"] = "��������: ";
$MESS["OWL2SLIDER_NE_ISPOLQZOVATQ"] = "�� ������������";
$MESS["OWL2SLIDER_POLE_DLA_SORTIROVKI"] = "���� ��� ���������� ";
$MESS["OWL2SLIDER_NAPRAVLENIE_SORTIROV"] = "����������� ���������� ";

$MESS["OWL2SLIDER_SOURCE_TYPE"] = "�������� ���������";
$MESS["OWL2SLIDER_SOURCE_TYPE_BANNERS"] = "������";
$MESS["OWL2SLIDER_SOURCE_TYPE_IBLOCK"] = "���������";

$MESS["OWL2SLIDER_IBLOCK_TYPE"] = "��� ���������";
$MESS["OWL2SLIDER_IBLOCK_ID"] = "��������";
$MESS["OWL2SLIDER_SECTION_ID"] = "������";

$MESS["OWL2SLIDER_ADVERT_TYPE"] = "��� ��������";
$MESS["OWL2SLIDER_NONE"] = "";

// - OWL 2 Options
$MESS["OWL2SLIDER_UNIQUE_SUFFIX"] = "���������� ��� ��� ���������� ������ (����� ����� �� ����.)";
$MESS["OWL2SLIDER_OWL_OPTS_visibleItemsCount"] = "���������� ������� ������� �� ������.";
$MESS["OWL2SLIDER_OWL_OPTS_margin"] = "�������� ������� ������ ��� ������� ������ (px)";
$MESS["OWL2SLIDER_OWL_OPTS_loop"] = "���������� ������ ����������";
$MESS["OWL2SLIDER_OWL_OPTS_center"] = "������������ ������ � ��������";
$MESS["OWL2SLIDER_OWL_OPTS_mouseDrag"] = "���������: ������������� � ������� ����.";
$MESS["OWL2SLIDER_OWL_OPTS_touchDrag"] = "���������: ������������� �� ��������� ������� �������.";
$MESS["OWL2SLIDER_OWL_OPTS_pullDrag"] = "Stage pull to edge.";
$MESS["OWL2SLIDER_OWL_OPTS_freeDrag"] = "��� �������������� ��������, ������������ ������ ��������.";
$MESS["OWL2SLIDER_OWL_OPTS_stagePadding"] = "���������� ������ �������� (��������� ������� '�������')(px)";
$MESS["OWL2SLIDER_OWL_OPTS_merge"] = "������������ ����������� �������.";
$MESS["OWL2SLIDER_OWL_OPTS_mergeFit"] = "���� ������ ������������ ������� ������ ������� ��������,�� ��������� ��������� ������ � ������� ��������.";
$MESS["OWL2SLIDER_OWL_OPTS_mergeSource"] = "�������� �������� ���������� ������������ �������.";
$MESS["OWL2SLIDER_OWL_OPTS_autoWidth"] = "autoWidth";
$MESS["OWL2SLIDER_OWL_OPTS_autoHeight"] = "����-������������ �� ������ (������ ��� 1 �������� ��������)";
$MESS["OWL2SLIDER_OWL_OPTS_startPosition"] = "������ ����� ������� � ������� �:";
$MESS["OWL2SLIDER_OWL_OPTS_nav"] = "�������� ������������� ������ (�����/������)";
$MESS["OWL2SLIDER_OWL_OPTS_navRewind"] = "Go to first/last.";

$MESS["OWL2SLIDER_OWL_OPTS_slideBy"] = "���������� �������������� �������. (���� �� �������, �� ��������� �� ���������)";
$MESS["OWL2SLIDER_OWL_OPTS_dots"] = "���������� ����� '���������'(Pagination)";
$MESS["OWL2SLIDER_OWL_OPTS_dotsEach"] = "�������� ����� '���������'(Pagination) ��� ������� ������";
$MESS["OWL2SLIDER_OWL_OPTS_dotData"] = "Used by data-dot content.";
$MESS["OWL2SLIDER_OWL_OPTS_lazyLoad"] = "��������� ����������� ������ ������� ������� (�������� LazyLoad) ";
$MESS["OWL2SLIDER_OWL_OPTS_autoplay"] = "����-����� ������ �������";
$MESS["OWL2SLIDER_OWL_OPTS_autoplayTimeout"] = "�������� ����� ��������� ������� ����-������ (��)";
$MESS["OWL2SLIDER_OWL_OPTS_autoplayHoverPause"] = "������������� ������� ��� ��������� ����� (hover ������)";
$MESS["OWL2SLIDER_OWL_OPTS_smartSpeed"] = "Speed Calculate. More info to come..";
$MESS["OWL2SLIDER_OWL_OPTS_autoplaySpeed"] = "�������� ����� ������� ��� ����-������ (��)";
$MESS["OWL2SLIDER_OWL_OPTS_navSpeed"] = "�������� ����� ������� ��� ����� �� ������������� ������ (��)";
$MESS["OWL2SLIDER_OWL_OPTS_dotsSpeed"] = "�������� ����� ������� ��� ����� ��  ������ '���������'(Pagination) (��)";
$MESS["OWL2SLIDER_OWL_OPTS_dragEndSpeed"] = "�������� ����� ������� ��� �������������� (Drag) (��).";

$MESS["OWL2SLIDER_OWL_OPTS_responsive"] = "�������� ����������(responsive) �����.";
$MESS["OWL2SLIDER_OWL_OPTS_responsiveMobileCnt"] = "���������� ������� ������� �� ���������";
$MESS["OWL2SLIDER_OWL_OPTS_responsiveTabletCnt"] = "���������� ������� ������� �� ���������";
$MESS["OWL2SLIDER_OWL_OPTS_responsiveLaptopCnt"] = "���������� ������� ������� �� ���������";
$MESS["OWL2SLIDER_OWL_OPTS_responsiveDesktopCnt"] = "���������� ������� ������� �� ���������";
$MESS["OWL2SLIDER_OWL_OPTS_responsiveRefreshRate"] = "������� ���������� Responsive (��)";
$MESS["OWL2SLIDER_OWL_OPTS_responsiveBaseElement"] = "��������� �������� �������� ������� � Responsive ������ � DOM ��������.";


$MESS["OWL2SLIDER_OWL_OPTS_video"] = "�������� ����� ����� �������� YouTube/Vimeo.";
$MESS["OWL2SLIDER_OWL_OPTS_videoHeight"] = "������ ����� ��������.";
$MESS["OWL2SLIDER_OWL_OPTS_videoWidth"] = "������ ����� ��������.";
$MESS["OWL2SLIDER_OWL_OPTS_videoSource"] = "�������� �������� ������ �� ����� �������.";

$MESS["OWL2SLIDER_OWL_OPTS_animateOut"] = "������������ CSS3 �������� '�������' ������.";
$MESS["OWL2SLIDER_OWL_OPTS_animateIn"] = "������������ CSS3 �������� '������' ������.";
$MESS["OWL2SLIDER_OWL_OPTS_fallbackEasing"] = "���������� �������� CSS2 ��� ���������� ���������.";
$MESS["OWL2SLIDER_OWL_OPTS_animateOut_Type"] = "CSS3 �������� '�������' ������.(Demo 'http://daneden.github.io/animate.css/')";
$MESS["OWL2SLIDER_OWL_OPTS_animateIn_Type"] = "CSS3 �������� '������' ������. (Demo 'http://daneden.github.io/animate.css/')";

$MESS["OWL2SLIDER_OWL_OPTS_rtl"] = "���������� ����������� �������� ������� ������ �� ����";

$MESS["OWL2SLIDER_OWL_OPTS_NAVIGATION_TYPE"] = "��� ���������� ������������� ������";
$MESS["OWL2SLIDER_OWL_OPTS_NAVIGATION_TYPE_ARROWS"] = "���������";
$MESS["OWL2SLIDER_OWL_OPTS_NAVIGATION_TYPE_TEXT"] = "�������";
$MESS["OWL2SLIDER_OWL_OPTS_NAVIGATION_TEXT_BACK"] = "����� ��� ������ ����� (����� HTML)";
$MESS["OWL2SLIDER_OWL_OPTS_NAVIGATION_TEXT_BACK_TEXT"] = "<div class='arrow__prev'></div>";
$MESS["OWL2SLIDER_OWL_OPTS_NAVIGATION_TEXT_NEXT"] = "����� ��� ������ ������ (����� HTML)";
$MESS["OWL2SLIDER_OWL_OPTS_NAVIGATION_TEXT_NEXT_TEXT"] = "<div class='arrow__next'></div>";

$MESS["OWL2SLIDER_OWL_OPTS_showDescription"] = "�������� �������� � �������";
$MESS["OWL2SLIDER_OWL_OPTS_DESCRIPTION_TEXT_SOURCE"] = "�������� �������� ����� ��������";

$MESS["OWL2SLIDER_OWL_OPTS_USE_RESIZE"] = "��������� ��������� �����������.";
$MESS["OWL2SLIDER_OWL_OPTS_USE_RESIZER2"] = "������������ ������ Resizer2";
$MESS["OWL2SLIDER_OWL_OPTS_RESIZE_LIST_SET"] = "����� Resizer2";
$MESS["OWL2SLIDER_OWL_OPTS_RESIZE_WIDTH"] = "������������ ������ ����������� (px)";
$MESS["OWL2SLIDER_OWL_OPTS_RESIZE_HEIGHT"] = "������������ ������ ����������� (px)";
$MESS["OWL2SLIDER_OWL_OPTS_RESIZE_IS_PROPORTIONAL"] = "���������� ���������";

// TIPs
$MESS["OWL2SLIDER_OWL_OPTS_merge_TIP"] = "��������� ������� ����� �������� ����� 1-�� ������, �������� ����� �������� ��������� ������ �� �������� ������� ����� ����� ���� � ����� ���������� ����� � ��������. ��. ���� ������: '���������� ����� ������������ ���������'";
$MESS["OWL2SLIDER_OWL_OPTS_stagePadding_TIP"] = "���� �������� ����� ������ ������ � ����� �� ��������� ���-�� (px). ��� ��������� ������� �������� ������";
$MESS["OWL2SLIDER_OWL_OPTS_lazyLoad_TIP"] = "��� ������ � ������� �������� ���������� �������� ��� �������� �������� ����������. ��� ����� ��������� ������� �������� �������� ������.";
$MESS["OWL2SLIDER_OWL_OPTS_responsive_TIP"] = "�����: ��� ��������� ���� �����. �������� '���������� ������� ������� �� ������.' ��������� �����������! ��� �������� ��������� �������� � ��������� ������.";
$MESS["OWL2SLIDER_LINK_URL_PROPERTY_ID_TIP"] = "���� �������. �� ��� ����� �� ����� ���������� �������� �� ������ �� ��������� � ���� �����";
$MESS["OWL2SLIDER_EXPERT_MODE_TIP"] = "��������� ����� ����� ��������� ��������� ��������. ��� ������������.";
$MESS["OWL2SLIDER_OWL_OPTS_video_TIP"] = "�������� ����� ���������� �������� ��� �������� ���� �� �������� ����� 
����� ������ �� �����. � ������ ���������� �����. ";